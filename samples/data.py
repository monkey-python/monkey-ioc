#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Person:

    def __init__(self, first_name, last_name, birth_date, death_date):
        self.first_name = first_name
        self.last_name = last_name
        self.birth_date = birth_date
        self.death_date = death_date

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, self.__class__):
            return self.first_name == other.first_name and \
                   self.last_name == other.last_name and \
                   self.birth_date == other.birth_date and \
                   self.death_date == other.death_date
        return False

    def __ne__(self, other):
        """Override the default Unequal behavior"""
        if isinstance(other, self.__class__):
            return self.first_name != other.first_name or \
                   self.last_name != other.last_name or \
                   self.birth_date != other.birth_date or \
                   self.death_date != other.death_date
        return True


class Posse:

    def __init__(self, members):
        self.members = members


class Gun:

    def __init__(self, name: str, designer: str, manufacturer: str, model: str, year: int):
        self.name = name
        self.designer = designer
        self.manufacturer = manufacturer
        self.model = model
        self.year = year


class Handgun(Gun):
    _SINGLE_ACTION_REVOLVER = 'Single Action Revolver'
    _DOUBLE_ACTION_REVOLVER = 'Double Action Revolver'

    def __init__(self, name: str, designer: str, manufacturer: str, model: str, year: int, type):
        super().__init__(name, designer, manufacturer, model, year)
        self.type = type
