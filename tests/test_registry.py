#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import datetime

from unittest import TestCase

from monkey.ioc.core import Registry

from samples.data import Person


class RegistryTest(TestCase):

    def setUp(self):
        self.registry = Registry()
        self.registry.load('samples/gunslingers.json')

    def tearDown(self) -> None:
        self.registry = None

    def test_get_none_key(self):
        value = self.registry.get(None)
        self.assertIsNone(value)

    def test_get_non_existing_key(self):
        value = self.registry.get('NON_EXISTING_KEY')
        self.assertIsNone(value)

    def test_get_non_existing_key_with_default(self):
        value = self.registry.get('NON_EXISTING_KEY', 'DEFAULT')
        self.assertEqual(value, 'DEFAULT')

    def test_get_string_element(self):
        value = self.registry.get('city', 'Unknown city')
        self.assertEqual(value, 'Tombstone')

    def test_get_int_element(self):
        value = self.registry.get('year', 0)
        self.assertEqual(value, 1873)

    def test_get_envvar_element(self):
        value = self.registry.get('username', 'Faked user')
        self.assertEqual(value, os.environ['USERNAME'])

    def test_get_date_element(self):
        value = self.registry.get('gunfight_at_the_ok_corral')
        self.assertEqual(value, datetime.date(1881, 10, 26))

    def test_get_time_element(self):
        value = self.registry.get('ten_past_ten_pm')
        self.assertEqual(value, datetime.time(22, 10, 00))

    def test_get_datetime_element(self):
        value = self.registry.get('first_moon_walk')
        self.assertEqual(value, datetime.datetime(1969, 7, 21, 2, 56))

    def test_get_object_element(self):
        wyatt_earp = Person('Wyatt', 'EARP', datetime.date(1848, 3, 19), datetime.date(1929, 1, 13))
        value = self.registry.get('wyatt_earp')
        self.assertEqual(value, wyatt_earp, msg='Not yet implemented')

    def test_get_ref_element(self):
        value = self.registry.get('wyatt_earp_ref', 0)
        referenced_value = self.registry.get('wyatt_earp', 1)
        self.assertEqual(value, referenced_value)

    def test_get_list_element(self):
        value = self.registry.get('earp_brothers')
        self.assertEqual(len(value), 3)

    def test_get_map_element(self):
        # TODO
        value = self.registry.get('xxx')
        self.assertEqual(value, 'xxx', msg='Not yet implemented')

    def test_get_dict_element(self):
        # TODO
        value = self.registry.get('xxx')
        self.assertEqual(value, 'xxx', msg='Not yet implemented')

    def test_get_list_parameter(self):
        tombstone_police = self.registry.get('tombstone_police')
        self.assertEqual(len(tombstone_police.members), 4)

    def test_get_class_element(self):
        person_class = self.registry.get('person_class')
        self.assertTrue(isinstance(person_class, type))
        self.assertTrue(person_class.__name__, 'Person')

    def test_get_module_element(self):
        mod = self.registry.get('data_module')
        self.assertEqual(type(mod).__name__, 'module')
        self.assertEqual(mod.__name__, 'samples.data')
